﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Maintenance.Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace Maintenance.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AircraftController : ControllerBase
    {
        [HttpPost]
        [Route("{aircraftId:int}/duelist")]
        public Task<ActionResult<DueList>> DueList(
            [FromRoute] int aircraftId,
            [FromBody] IEnumerable<MaintenanceTaskInput> tasks)
        {
            return Task.FromResult<ActionResult<DueList>>(Ok(new DueList()));
        }
    }
}