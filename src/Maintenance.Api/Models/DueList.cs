using System.Collections.Generic;

namespace Maintenance.Api.Models
{
    public class DueList
    {
        public int AircraftId { get; set; }

        public List<DueListTask> Tasks { get; set; }
    }
}